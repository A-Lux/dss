<?php

namespace backend\controllers;

use Yii;
use common\models\Contact;
    use backend\models\search\ContactSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
* ContactController implements the CRUD actions for Contact model.
*/
class ContactController extends BackendController
{
    /**
    * @var array
    */
    protected $permissions = [
        'create' => 'createContact',
        'view'   => 'viewContact',
        'update' => 'updateContact',
        'index'  => 'indexContact',
        'delete' => 'deleteContact',
    ];

    /**
    * @param Action $action
    * @return bool
    * @throws
    */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Contact::className();
            $this->searchModel = ContactSearch::className();

            return true;
        }

        return false;
    }
}
