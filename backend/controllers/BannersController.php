<?php

namespace backend\controllers;

use common\models\MultilingualActiveRecord;
use Yii;
use common\models\Banners;
use backend\models\search\BannersSearch;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
* BannersController implements the CRUD actions for Banners model.
*/
class BannersController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createBanner',
        'view'   => 'viewBanner',
        'update' => 'updateBanner',
        'index'  => 'indexBanner',
        'delete' => 'deleteBanner',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Banners::className();
            $this->searchModel = BannersSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Creates a new Banners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (false === \Yii::$app->user->can($this->permissions['create'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на создание');
        }
        $model = new Banners();

        if ($model->load(Yii::$app->request->post())) {

            $this->createImage($model);

            if($model->save()) {
                \Yii::$app->session->setFlash('success', 'Объект успешно создан и сохранен!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Banners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }
        $model = $this->findModel($id);
        $oldImage = $model->image;

        if ($model->load(Yii::$app->request->post())) {

            $this->updateImage($model, $oldImage);
            if($model->save()) {
                \Yii::$app->session->setFlash('info', 'Изменения приняты!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Banners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = Banners::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
