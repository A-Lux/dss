<?php

namespace backend\controllers;

use common\models\MultilingualActiveRecord;
use Yii;
use common\models\SubcategoryDocuments;
    use backend\models\search\SubcategoryDocumentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
* SubcategoryDocumentsController implements the CRUD actions for SubcategoryDocuments model.
*/
class SubcategoryDocumentsController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createAbout',
        'view'   => 'viewAbout',
        'update' => 'updateAbout',
        'index'  => 'indexAbout',
        'delete' => 'deleteAbout',
    ];

    /**
    * @param Action $action
    * @return bool
    * @throws
    */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = SubcategoryDocuments::className();
            $this->searchModel = SubcategoryDocumentsSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Finds the SubcategoryDocuments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = SubcategoryDocuments::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
