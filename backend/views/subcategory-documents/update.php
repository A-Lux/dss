<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SubcategoryDocuments */

$this->title = 'Редактировать Subcategory Documents: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Subcategory Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="subcategory-documents-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
