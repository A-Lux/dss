<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SubcategoryDocuments */

$this->title = 'Создание Subcategory Documents';
$this->params['breadcrumbs'][] = ['label' => 'Subcategory Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subcategory-documents-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
