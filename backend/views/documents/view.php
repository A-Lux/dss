<?php

use yii\helpers\Html;
use common\widgets\MultilingualDetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Documents */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="documents-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php $files = $model->getFile(); ?>
    <?php
    if($files != null)
        $file = ['attribute'=>'file','value' => Html::a($model->file,$files,['target'=>'_blank']),'format' => 'raw' ];
    else
        $file =  ['attribute'=>'file','value' => '']?>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return
                        Html::a($model->category->name, ['view', 'id' => $model->category->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'subcategory_id',
                'value' => function ($model) {
                    return !is_null($model->subcategory)
                        ? Html::a($model->subcategory->name, ['view', 'id' => $model->subcategory->id])
                        : ' ';
                },
                'format' => 'raw',
            ],
            'name',
            $file,
            'created_at',
        ],
    ]) ?>

</div>
