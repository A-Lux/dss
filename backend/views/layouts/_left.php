<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'По сайту', 'options' => ['class' => 'header']],
                    ['label' => 'Меню', 'icon' => 'bookmark-o', 'url' => ['/menu']],
                    ['label' => 'О нас', 'icon' => 'bookmark-o', 'url' => ['/about']],
                    ['label' => 'Преимущество', 'icon' => 'bookmark-o', 'url' => ['/advantages']],
                    ['label' => 'Галерея', 'icon' => 'bookmark-o', 'url' => ['/gallery']],
                    ['label' => 'Объекты', 'icon' => 'bookmark-o', 'url' => ['/objects']],
                    [
                        'label' => 'Документы',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Категории', 'icon' => 'ship', 'url' => ['/category-documents']],
                            ['label' => 'Подкатегории', 'icon' => 'ship', 'url' => ['/subcategory-documents']],
                            ['label' => 'Документы', 'icon' => 'th', 'url' => ['/documents'],],
                            ['label' => 'Карточки', 'icon' => 'th', 'url' => ['/document-cart'],],
                        ],
                    ],
                    ['label' => 'Гос. символы', 'icon' => 'bookmark-o', 'url' => ['/symbols']],
                    ['label' => 'Сотрудники', 'icon' => 'bookmark-o', 'url' => ['/team']],
                    ['label' => 'Контакты', 'icon' => 'puzzle-piece', 'url' => ['/contact']],
                ],
            ]
        ) ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Основное', 'options' => ['class' => 'header']],
                    ['label' => 'Переводы', 'icon' => 'language', 'url' => ['/source-message/']],
//                    ['label' => 'Пользователи', 'icon' => 'user-circle-o', 'url' => ['/users']],
                    ['label' => 'Язык', 'icon' => 'language', 'url' => ['/language']],
                    ['label' => 'Баннер', 'icon' => 'columns', 'url' => ['/banners']],
                    ['label' => 'Логотип', 'icon' => 'image', 'url' => ['/logo']],
                    ['label' => 'Соц. сети', 'icon' => 'image', 'url' => ['/social-networks']],

                ],
            ]
        ) ?>

    </section>

</aside>
