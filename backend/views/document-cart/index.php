<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DocumentCartSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Document Carts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-cart-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать Document Cart', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return
                        Html::a($model->category->name, ['view', 'id' => $model->category->id]);
                },
                'format' => 'raw',
            ],
            'name',
            'description:ntext',
            'phone',
            //'image:ntext',
            //'sort',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
