<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DocumentCart */

$this->title = 'Создание Document Cart';
$this->params['breadcrumbs'][] = ['label' => 'Document Carts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-cart-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
