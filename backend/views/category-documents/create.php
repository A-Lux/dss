<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CategoryDocuments */

$this->title = 'Создание Category Documents';
$this->params['breadcrumbs'][] = ['label' => 'Category Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-documents-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
