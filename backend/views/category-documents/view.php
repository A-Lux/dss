<?php

use yii\helpers\Html;
use common\widgets\MultilingualDetailView;
use common\models\CategoryDocuments;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\CategoryDocuments */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Category Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="category-documents-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'status',
                'filter' => CategoryDocuments::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(CategoryDocuments::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'created_at',
        ],
    ]) ?>

</div>
