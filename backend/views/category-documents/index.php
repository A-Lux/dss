<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\CategoryDocuments;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CategoryDocumentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Category Documents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-documents-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать Category Documents', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'status',
                'filter' => CategoryDocuments::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(CategoryDocuments::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
