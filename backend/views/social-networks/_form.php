<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\SocialNetworks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="social-networks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload'            => false ,
            'initialPreview'        => $model->isNewRecord ? '' : $model->getImage(),
            'initialPreviewAsData'  => true,
            'initialCaption'        => $model->isNewRecord ? '': $model->image,
            'showRemove'            => true ,
            'deleteUrl'             => \yii\helpers\Url::to(['/social-networks/delete-image', 'id'=> $model->id]),
        ] ,'options' => ['accept' => 'image/*'],
    ]); ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
