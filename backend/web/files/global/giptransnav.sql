-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: srv-pleskdb37.ps.kz:3306
-- Время создания: Апр 03 2020 г., 14:59
-- Версия сервера: 10.2.31-MariaDB
-- Версия PHP: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `giptrans_nav`
--

-- --------------------------------------------------------

--
-- Структура таблицы `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `abouts`
--

INSERT INTO `abouts` (`id`, `first_content`, `second_content`, `image`, `video`, `created_at`, `updated_at`) VALUES
(1, 'Охранное агентство Navigator-SG предлагает широкий комплекс охранных систем и услуг в Алматы и по всей территории РК.', '<p class=\"description\">Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам. Navigator-SG &ndash; одна из крупнейших компаний&ndash;поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.</p>\r\n<p class=\"description\">Где-то лучше подойдёт пультовая охрана, где-то оптимальным вариантом будет организация видеонаблюдения или же использование активных охранных систем.</p>', 'abouts\\March2020\\eONuOBgAWTz8ykq8Sjz4.png', '[{\"download_link\":\"abouts\\\\March2020\\\\42ZqfKE0nGDUDx6vU8ke.mp4\",\"original_name\":\"mov_bbb.mp4\"}]', '2020-03-27 02:49:35', '2020-03-27 02:49:35');

-- --------------------------------------------------------

--
-- Структура таблицы `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `background` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `banners`
--

INSERT INTO `banners` (`id`, `background`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 'banners\\March2020\\gM2VYB6ucTsD4SRiQCmF.png', 'Охранное агенство Navigator-SG', 'Мы предлагаем клиентам только надежные и проверенные решения для организации охраны объектов. Специалисты компании оказывают помощь в подборе наиболее подходящего варианта для конкретного случая.', '2020-03-27 02:44:07', '2020-03-27 02:44:07');

-- --------------------------------------------------------

--
-- Структура таблицы `benefits`
--

CREATE TABLE `benefits` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `benefits`
--

INSERT INTO `benefits` (`id`, `text`, `created_at`, `updated_at`, `sort`) VALUES
(1, 'Широкий спектр услуг', '2020-03-27 02:51:59', '2020-03-27 02:52:31', 1),
(2, 'Высококвалифицированные специалисты', '2020-03-27 02:52:05', '2020-03-27 02:52:31', 2),
(3, 'Служба технической поддержки', '2020-03-27 02:52:12', '2020-03-27 02:52:31', 3),
(4, 'Индивидуальный подход', '2020-03-27 02:52:19', '2020-03-27 02:52:31', 4),
(5, 'Гибкая система оплаты', '2020-03-27 02:52:24', '2020-03-27 02:52:31', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'background', 'image', 'Картинка', 1, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'title', 'text', 'Заголовок', 1, 1, 1, 1, 1, 1, '{}', 3),
(25, 4, 'content', 'text_area', 'Подзаголовок', 0, 1, 1, 1, 1, 1, '{}', 4),
(26, 4, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 5),
(27, 4, 'updated_at', 'timestamp', 'Дата редактирования', 0, 0, 1, 0, 0, 0, '{}', 6),
(28, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(29, 5, 'first_content', 'text_area', 'Первый блок', 0, 1, 1, 1, 1, 1, '{}', 2),
(30, 5, 'second_content', 'rich_text_box', 'Второй блок', 0, 1, 1, 1, 1, 1, '{}', 3),
(31, 5, 'image', 'image', 'Картинка', 0, 1, 1, 1, 1, 1, '{}', 4),
(32, 5, 'video', 'file', 'Видео', 0, 1, 1, 1, 1, 1, '{}', 5),
(33, 5, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 6),
(34, 5, 'updated_at', 'timestamp', 'Дата редактирования', 0, 0, 1, 0, 0, 0, '{}', 7),
(35, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(36, 6, 'text', 'text', 'Текст', 1, 1, 1, 1, 1, 1, '{}', 2),
(37, 6, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 3),
(38, 6, 'updated_at', 'timestamp', 'Дата редактирования', 0, 0, 1, 0, 0, 0, '{}', 4),
(39, 6, 'sort', 'text', 'Сортировка', 0, 0, 0, 0, 0, 0, '{}', 5),
(40, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(41, 7, 'title', 'text', 'Заголовок', 1, 1, 1, 1, 1, 1, '{}', 2),
(42, 7, 'subtitle', 'text', 'Подзаголовок', 0, 1, 1, 1, 1, 1, '{}', 3),
(44, 7, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 5),
(45, 7, 'updated_at', 'timestamp', 'Дата редактирования', 0, 0, 1, 0, 0, 0, '{}', 6),
(46, 7, 'sort', 'text', 'Сортировка', 0, 0, 0, 0, 0, 0, '{}', 7),
(47, 7, 'class', 'text', 'Класс', 1, 1, 1, 1, 1, 1, '{}', 4),
(48, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(49, 8, 'fio', 'text', 'ФИО', 1, 1, 1, 1, 1, 1, '{}', 2),
(50, 8, 'image', 'image', 'Картинка', 0, 1, 1, 1, 1, 1, '{}', 3),
(51, 8, 'review', 'text_area', 'Отзыв', 1, 0, 1, 1, 1, 1, '{}', 4),
(52, 8, 'date', 'date', 'Дата', 1, 1, 1, 1, 1, 1, '{}', 5),
(53, 8, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 6),
(54, 8, 'updated_at', 'timestamp', 'Дата редактирования', 0, 0, 1, 0, 0, 0, '{}', 7),
(55, 8, 'sort', 'text', 'Сортировка', 0, 0, 0, 0, 0, 0, '{}', 8),
(56, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 9, 'fio', 'text', 'ФИО', 1, 1, 1, 1, 1, 1, '{}', 2),
(58, 9, 'phone', 'text', 'Телефон', 1, 1, 1, 1, 1, 1, '{}', 3),
(59, 9, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 4),
(60, 9, 'updated_at', 'timestamp', 'Дата редактирования', 0, 0, 1, 0, 0, 0, '{}', 5),
(61, 9, 'sort', 'text', 'Сортировка', 0, 0, 0, 0, 0, 0, '{}', 6),
(62, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(63, 10, 'meta_title', 'text', 'Мета заголовок', 1, 1, 1, 1, 1, 1, '{}', 2),
(64, 10, 'meta_description', 'text', 'Мета описание', 0, 1, 1, 1, 1, 1, '{}', 3),
(65, 10, 'meta_keyword', 'text', 'Ключевое слово', 0, 1, 1, 1, 1, 1, '{}', 4),
(66, 10, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 1, 0, 1, '{}', 5),
(67, 10, 'updated_at', 'timestamp', 'Дата редактирования', 0, 0, 1, 0, 0, 0, '{}', 6),
(68, 10, 'url', 'text', 'Ссылка', 1, 1, 1, 0, 1, 1, '{}', 7);

-- --------------------------------------------------------

--
-- Структура таблицы `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(4, 'banners', 'banners', 'Баннер', 'Баннер', NULL, 'App\\Banner', NULL, 'App\\Http\\Controllers\\Admin\\BannerController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-03-27 02:40:51', '2020-03-27 02:45:10'),
(5, 'abouts', 'abouts', 'О компании', 'О компании', NULL, 'App\\About', NULL, 'App\\Http\\Controllers\\Admin\\AboutController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-03-27 02:46:55', '2020-03-27 02:50:03'),
(6, 'benefits', 'benefits', 'Преимущества', 'Преимущества', NULL, 'App\\Benefit', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"sort\",\"order_display_column\":\"text\",\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-03-27 02:51:43', '2020-03-27 02:51:43'),
(7, 'services', 'services', 'Услуга', 'Наши услуги', NULL, 'App\\Service', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"sort\",\"order_display_column\":\"title\",\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-03-27 02:54:59', '2020-03-27 03:09:42'),
(8, 'reviews', 'reviews', 'Отзыв', 'Отзывы клиентов', NULL, 'App\\Review', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"sort\",\"order_display_column\":\"fio\",\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-03-27 03:15:13', '2020-03-27 03:15:13'),
(9, 'feedback', 'feedback', 'Обратная связь', 'Обратная связь', NULL, 'App\\Feedback', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"sort\",\"order_display_column\":\"fio\",\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-03-27 03:25:53', '2020-03-30 09:12:05'),
(10, 'pages', 'pages', 'Страница', 'Страницы', NULL, 'App\\Page', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-03-27 04:16:38', '2020-03-27 04:22:02');

-- --------------------------------------------------------

--
-- Структура таблицы `feedback`
--

CREATE TABLE `feedback` (
  `id` int(10) UNSIGNED NOT NULL,
  `fio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `feedback`
--

INSERT INTO `feedback` (`id`, `fio`, `phone`, `created_at`, `updated_at`, `sort`) VALUES
(2, 'Нурзат', '+77773562536', '2020-03-30 09:16:50', '2020-03-30 09:16:50', NULL),
(3, 'Test', '+77088546554', '2020-03-30 09:19:11', '2020-03-30 09:19:11', NULL),
(4, 'Полина Олеговна Блощицина', '+77076365966', '2020-04-02 09:48:08', '2020-04-02 09:48:08', NULL),
(5, 'Полина Олеговна Блощицина', '+77076365966', '2020-04-02 09:48:18', '2020-04-02 09:48:18', NULL),
(6, 'Nurzat Yerketaev', '+77245456463sad', '2020-04-02 09:49:49', '2020-04-02 09:49:49', NULL),
(7, 'asdasdas', '+77088101234', '2020-04-02 09:50:06', '2020-04-02 09:50:06', NULL),
(8, 'Test', '+77088191463', '2020-04-02 10:05:00', '2020-04-02 10:05:00', NULL),
(9, 'test', '+77774562523', '2020-04-02 10:07:40', '2020-04-02 10:07:40', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(2, 'site', '2020-03-27 00:53:42', '2020-03-27 00:53:42');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Панель управления', '', '_self', 'voyager-boat', '#000000', NULL, 1, '2020-03-26 22:39:57', '2020-03-27 00:33:55', 'voyager.dashboard', 'null'),
(2, 1, 'Медиа', '', '_self', 'voyager-images', '#000000', NULL, 4, '2020-03-26 22:39:57', '2020-03-27 00:38:53', 'voyager.media.index', 'null'),
(3, 1, 'Пользователей', '', '_self', 'voyager-person', '#000000', NULL, 3, '2020-03-26 22:39:57', '2020-03-27 00:38:38', 'voyager.users.index', 'null'),
(4, 1, 'Роли', '', '_self', 'voyager-lock', '#000000', NULL, 2, '2020-03-26 22:39:57', '2020-03-27 00:38:28', 'voyager.roles.index', 'null'),
(5, 1, 'Инструменты', '', '_self', 'voyager-tools', '#000000', NULL, 5, '2020-03-26 22:39:57', '2020-03-27 00:39:21', NULL, ''),
(6, 1, 'Конструктор меню', '', '_self', 'voyager-list', '#000000', 5, 1, '2020-03-26 22:39:57', '2020-03-27 00:49:00', 'voyager.menus.index', 'null'),
(7, 1, 'База данных', '', '_self', 'voyager-data', '#000000', 5, 2, '2020-03-26 22:39:57', '2020-03-27 00:49:35', 'voyager.database.index', 'null'),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-03-26 22:39:57', '2020-03-27 00:38:24', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-03-26 22:39:57', '2020-03-27 00:38:24', 'voyager.bread.index', NULL),
(10, 1, 'Настройки', '', '_self', 'voyager-settings', '#000000', NULL, 6, '2020-03-26 22:39:57', '2020-03-27 00:49:46', 'voyager.settings.index', 'null'),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-03-26 22:39:58', '2020-03-27 00:38:24', 'voyager.hooks', NULL),
(12, 2, 'Главная', '/#home', '_self', NULL, '#000000', NULL, 7, '2020-03-27 00:54:35', '2020-03-27 00:54:35', NULL, ''),
(13, 2, 'О компании', '/#about', '_self', NULL, '#000000', NULL, 8, '2020-03-27 00:55:03', '2020-03-27 00:55:03', NULL, ''),
(14, 2, 'Услуги', '/#services', '_self', NULL, '#000000', NULL, 9, '2020-03-27 00:55:20', '2020-03-27 00:55:20', NULL, ''),
(15, 2, 'Отзывы', '/#reviews', '_self', NULL, '#000000', NULL, 10, '2020-03-27 00:55:34', '2020-03-27 00:55:34', NULL, ''),
(16, 2, 'Контакты', '/#contacts', '_self', NULL, '#000000', NULL, 11, '2020-03-27 00:56:01', '2020-03-27 00:56:01', NULL, ''),
(17, 1, 'Баннер', '', '_self', 'voyager-categories', '#000000', NULL, 8, '2020-03-27 02:40:51', '2020-03-27 04:19:12', 'voyager.banners.index', 'null'),
(18, 1, 'О компании', '', '_self', 'voyager-company', '#000000', NULL, 9, '2020-03-27 02:46:55', '2020-03-27 04:19:12', 'voyager.abouts.index', 'null'),
(19, 1, 'Преимущества', '', '_self', 'voyager-diamond', '#000000', NULL, 10, '2020-03-27 02:51:43', '2020-03-27 04:19:12', 'voyager.benefits.index', 'null'),
(20, 1, 'Наши услуги', '', '_self', 'voyager-treasure-open', '#000000', NULL, 11, '2020-03-27 02:55:00', '2020-03-27 04:19:12', 'voyager.services.index', 'null'),
(21, 1, 'Отзывы клиентов', '', '_self', 'voyager-chat', '#000000', NULL, 12, '2020-03-27 03:15:14', '2020-03-27 04:19:12', 'voyager.reviews.index', 'null'),
(22, 1, 'Обратная связь', '', '_self', 'voyager-mail', '#000000', NULL, 13, '2020-03-27 03:25:53', '2020-03-27 04:19:12', 'voyager.feedback.index', 'null'),
(23, 1, 'Страницы', '', '_self', 'voyager-list', '#000000', NULL, 7, '2020-03-27 04:16:39', '2020-03-27 04:19:12', 'voyager.pages.index', 'null');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keyword` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `meta_title`, `meta_description`, `meta_keyword`, `created_at`, `updated_at`, `url`) VALUES
(1, 'Охранное агентство Navigator-SG', 'Охранное агентство Navigator-SG', 'ОХРАННО-ТРЕВОЖНАЯ СИГНАЛИЗАЦИЯ (ОТС), СКС И ВОЛС, ВИДЕОНАБЛЮДЕНИЕ, ПОЖАРНАЯ СИГНАЛИЗАЦИЯ, СИСТЕМЫ КОНТРОЛЯ ДОСТУПА, УСТАНОВКА ШЛАГБАУМОВ, ТЕХНИЧЕСКОЕ ОБСЛУЖИВАНИЕ, БРОНИРОВАНИЕ CТЕКОЛ', '2020-03-27 04:17:00', '2020-04-02 10:24:04', NULL),
(2, 'Охранное агентство Navigator-SG', 'Охранное агентство Navigator-SG', 'ОХРАННО-ТРЕВОЖНАЯ СИГНАЛИЗАЦИЯ (ОТС), СКС И ВОЛС, ВИДЕОНАБЛЮДЕНИЕ, ПОЖАРНАЯ СИГНАЛИЗАЦИЯ, СИСТЕМЫ КОНТРОЛЯ ДОСТУПА, УСТАНОВКА ШЛАГБАУМОВ, ТЕХНИЧЕСКОЕ ОБСЛУЖИВАНИЕ, БРОНИРОВАНИЕ CТЕКОЛ', '2020-03-27 04:17:00', '2020-04-02 10:25:12', 'search');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(2, 'browse_bread', NULL, '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(3, 'browse_database', NULL, '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(4, 'browse_media', NULL, '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(5, 'browse_compass', NULL, '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(6, 'browse_menus', 'menus', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(7, 'read_menus', 'menus', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(8, 'edit_menus', 'menus', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(9, 'add_menus', 'menus', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(10, 'delete_menus', 'menus', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(11, 'browse_roles', 'roles', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(12, 'read_roles', 'roles', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(13, 'edit_roles', 'roles', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(14, 'add_roles', 'roles', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(15, 'delete_roles', 'roles', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(16, 'browse_users', 'users', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(17, 'read_users', 'users', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(18, 'edit_users', 'users', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(19, 'add_users', 'users', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(20, 'delete_users', 'users', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(21, 'browse_settings', 'settings', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(22, 'read_settings', 'settings', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(23, 'edit_settings', 'settings', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(24, 'add_settings', 'settings', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(25, 'delete_settings', 'settings', '2020-03-26 22:39:57', '2020-03-26 22:39:57'),
(26, 'browse_hooks', NULL, '2020-03-26 22:39:58', '2020-03-26 22:39:58'),
(27, 'browse_banners', 'banners', '2020-03-27 02:40:51', '2020-03-27 02:40:51'),
(28, 'read_banners', 'banners', '2020-03-27 02:40:51', '2020-03-27 02:40:51'),
(29, 'edit_banners', 'banners', '2020-03-27 02:40:51', '2020-03-27 02:40:51'),
(30, 'add_banners', 'banners', '2020-03-27 02:40:51', '2020-03-27 02:40:51'),
(31, 'delete_banners', 'banners', '2020-03-27 02:40:51', '2020-03-27 02:40:51'),
(32, 'browse_abouts', 'abouts', '2020-03-27 02:46:55', '2020-03-27 02:46:55'),
(33, 'read_abouts', 'abouts', '2020-03-27 02:46:55', '2020-03-27 02:46:55'),
(34, 'edit_abouts', 'abouts', '2020-03-27 02:46:55', '2020-03-27 02:46:55'),
(35, 'add_abouts', 'abouts', '2020-03-27 02:46:55', '2020-03-27 02:46:55'),
(36, 'delete_abouts', 'abouts', '2020-03-27 02:46:55', '2020-03-27 02:46:55'),
(37, 'browse_benefits', 'benefits', '2020-03-27 02:51:43', '2020-03-27 02:51:43'),
(38, 'read_benefits', 'benefits', '2020-03-27 02:51:43', '2020-03-27 02:51:43'),
(39, 'edit_benefits', 'benefits', '2020-03-27 02:51:43', '2020-03-27 02:51:43'),
(40, 'add_benefits', 'benefits', '2020-03-27 02:51:43', '2020-03-27 02:51:43'),
(41, 'delete_benefits', 'benefits', '2020-03-27 02:51:43', '2020-03-27 02:51:43'),
(42, 'browse_services', 'services', '2020-03-27 02:55:00', '2020-03-27 02:55:00'),
(43, 'read_services', 'services', '2020-03-27 02:55:00', '2020-03-27 02:55:00'),
(44, 'edit_services', 'services', '2020-03-27 02:55:00', '2020-03-27 02:55:00'),
(45, 'add_services', 'services', '2020-03-27 02:55:00', '2020-03-27 02:55:00'),
(46, 'delete_services', 'services', '2020-03-27 02:55:00', '2020-03-27 02:55:00'),
(47, 'browse_reviews', 'reviews', '2020-03-27 03:15:14', '2020-03-27 03:15:14'),
(48, 'read_reviews', 'reviews', '2020-03-27 03:15:14', '2020-03-27 03:15:14'),
(49, 'edit_reviews', 'reviews', '2020-03-27 03:15:14', '2020-03-27 03:15:14'),
(50, 'add_reviews', 'reviews', '2020-03-27 03:15:14', '2020-03-27 03:15:14'),
(51, 'delete_reviews', 'reviews', '2020-03-27 03:15:14', '2020-03-27 03:15:14'),
(52, 'browse_feedback', 'feedbacks', '2020-03-27 03:25:53', '2020-03-27 03:25:53'),
(53, 'read_feedback', 'feedbacks', '2020-03-27 03:25:53', '2020-03-27 03:25:53'),
(54, 'edit_feedback', 'feedbacks', '2020-03-27 03:25:53', '2020-03-27 03:25:53'),
(55, 'add_feedback', 'feedbacks', '2020-03-27 03:25:53', '2020-03-27 03:25:53'),
(56, 'delete_feedback', 'feedbacks', '2020-03-27 03:25:53', '2020-03-27 03:25:53'),
(57, 'browse_feedback', 'feedback', '2020-03-27 03:29:55', '2020-03-27 03:29:55'),
(58, 'read_feedback', 'feedback', '2020-03-27 03:29:55', '2020-03-27 03:29:55'),
(59, 'edit_feedback', 'feedback', '2020-03-27 03:29:55', '2020-03-27 03:29:55'),
(60, 'add_feedback', 'feedback', '2020-03-27 03:29:55', '2020-03-27 03:29:55'),
(61, 'delete_feedback', 'feedback', '2020-03-27 03:29:55', '2020-03-27 03:29:55'),
(62, 'browse_pages', 'pages', '2020-03-27 04:16:39', '2020-03-27 04:16:39'),
(63, 'read_pages', 'pages', '2020-03-27 04:16:39', '2020-03-27 04:16:39'),
(64, 'edit_pages', 'pages', '2020-03-27 04:16:39', '2020-03-27 04:16:39'),
(65, 'add_pages', 'pages', '2020-03-27 04:16:39', '2020-03-27 04:16:39'),
(66, 'delete_pages', 'pages', '2020-03-27 04:16:39', '2020-03-27 04:16:39');

-- --------------------------------------------------------

--
-- Структура таблицы `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 4),
(2, 1),
(3, 1),
(4, 1),
(4, 4),
(5, 1),
(5, 4),
(6, 1),
(6, 4),
(7, 1),
(7, 4),
(8, 1),
(8, 4),
(9, 1),
(9, 4),
(10, 1),
(10, 4),
(11, 1),
(11, 4),
(12, 1),
(12, 4),
(13, 1),
(13, 4),
(14, 1),
(14, 4),
(15, 1),
(15, 4),
(16, 1),
(16, 4),
(17, 1),
(17, 4),
(18, 1),
(18, 4),
(19, 1),
(19, 4),
(20, 1),
(20, 4),
(21, 1),
(21, 4),
(22, 1),
(22, 4),
(23, 1),
(23, 4),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(27, 4),
(28, 1),
(28, 4),
(29, 1),
(29, 4),
(30, 1),
(31, 1),
(32, 1),
(32, 4),
(33, 1),
(33, 4),
(34, 1),
(34, 4),
(35, 1),
(36, 1),
(37, 1),
(37, 4),
(38, 1),
(38, 4),
(39, 1),
(39, 4),
(40, 1),
(40, 4),
(41, 1),
(41, 4),
(42, 1),
(42, 4),
(43, 1),
(43, 4),
(44, 1),
(44, 4),
(45, 1),
(45, 4),
(46, 1),
(46, 4),
(47, 1),
(47, 4),
(48, 1),
(48, 4),
(49, 1),
(49, 4),
(50, 1),
(50, 4),
(51, 1),
(51, 4),
(52, 1),
(52, 4),
(53, 1),
(53, 4),
(54, 1),
(54, 4),
(55, 1),
(55, 4),
(56, 1),
(56, 4),
(57, 1),
(57, 4),
(58, 1),
(58, 4),
(59, 1),
(59, 4),
(60, 1),
(60, 4),
(61, 1),
(61, 4),
(62, 1),
(62, 4),
(63, 1),
(63, 4),
(64, 1),
(64, 4),
(65, 1),
(66, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `fio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `reviews`
--

INSERT INTO `reviews` (`id`, `fio`, `image`, `review`, `date`, `created_at`, `updated_at`, `sort`) VALUES
(1, 'Александр Ткачук 3', 'reviews\\March2020\\8VgG1MOBJNLo2gBpQzpe.png', 'Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам. компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.', '2020-03-24', '2020-03-27 03:16:00', '2020-03-27 03:17:29', 3),
(2, 'Александр Ткачук 2', 'reviews\\March2020\\2BTBY0U5cFfmyiwfjv3W.png', 'Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам. компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.', '2020-03-13', '2020-03-27 03:16:00', '2020-03-27 03:19:29', 2),
(3, 'Александр Ткачук 4', 'reviews\\March2020\\sxieuDZbv4290WNi9mQY.png', 'Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам. компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.', '2020-02-07', '2020-03-27 03:16:00', '2020-03-27 03:18:36', 4),
(4, 'Александр Ткачук 5', 'reviews\\March2020\\DtJ09KvePj6EWkchzgc4.png', 'Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам. компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.', '2020-03-24', '2020-03-27 03:16:00', '2020-03-27 03:17:27', 5),
(5, 'Александр Ткачук 6', 'reviews\\March2020\\V1oG2pYQ8Qo9hjJbGWOH.png', 'Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам. компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.', '2020-03-21', '2020-03-27 03:16:00', '2020-03-27 03:19:04', 6),
(6, 'Александр Ткачук 7', 'reviews\\March2020\\oiesZ9nfYdBh3NpQZkcC.png', 'Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам. компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.', '2020-03-24', '2020-03-27 03:16:00', '2020-03-27 03:17:27', 7),
(7, 'Александр Ткачук 8', 'reviews\\March2020\\8c9Q05xwDnKcN3JfhN7G.png', 'Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам. компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.', '2020-03-08', '2020-03-27 03:16:00', '2020-03-27 03:18:23', 8),
(8, 'Александр Ткачук', 'reviews\\March2020\\GmMTPq7Tb1xePqTVaEo8.png', 'Охрана квартир, домов, офисов и коттеджей по самым приемлемым ценам. компаний–поставщиков услуг в Алматы и всем регионе в области обеспечения охраны различных объектов. Мы продаем, монтируем, подключаем и обслуживаем охранные системы любых типов, как для бытовых, так и для промышленных объектов.', '2020-01-09', '2020-03-27 03:16:00', '2020-03-27 03:18:49', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'Developer', 'Developer', '2020-03-26 22:39:57', '2020-03-30 10:45:37'),
(4, 'Administrator', 'Admin', '2020-03-30 10:44:58', '2020-03-30 10:44:58');

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`id`, `title`, `subtitle`, `class`, `created_at`, `updated_at`, `sort`) VALUES
(1, 'Охранно-тревожная', 'сигнализация (отс)', 'fal fa-signal-stream', '2020-03-27 03:10:38', '2020-03-27 03:12:54', 1),
(2, 'Бронирование', 'cтекол', 'fal fa-layer-group', '2020-03-27 03:10:55', '2020-03-27 03:12:54', 2),
(3, 'Техническое', 'обслуживание', 'fal fa-cog', '2020-03-27 03:11:12', '2020-03-27 03:12:54', 3),
(4, 'Установка', 'шлагбаумов', 'fad fa-sliders-v', '2020-03-27 03:11:34', '2020-03-27 03:12:54', 4),
(5, 'Системы', 'контроля доступа', 'fal fa-unlock', '2020-03-27 03:11:55', '2020-03-27 03:12:54', 5),
(6, 'Пожарная', 'сигнализация', 'fal fa-shield', '2020-03-27 03:12:11', '2020-03-27 03:12:54', 6),
(7, 'Система', 'видеонаблюдения', 'fal fa-video', '2020-03-27 03:12:29', '2020-03-27 03:12:54', 7),
(8, 'СКС и ВОЛС', NULL, 'fal fa-scrubber', '2020-03-27 03:12:43', '2020-03-27 03:12:54', 8);

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Navigator', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Добро пожаловать в Navigator.', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
(11, 'site.logotyp', 'Логотип', 'settings\\March2020\\1HUieuXrknH6A5CTfBlD.png', NULL, 'image', 6, 'Site'),
(12, 'site.copyright', 'Копирайт', 'Navigator SG ©2020 Все права защищены', NULL, 'text', 7, 'Site'),
(13, 'site.address', 'Адрес', 'г. Алматы, ул.Валиханова, 63', NULL, 'text', 8, 'Site'),
(14, 'site.telephone', 'Телефон', '+7 776 111 11 46', NULL, 'text', 9, 'Site'),
(15, 'site.email', 'E-mail', 'info@navigator-sc.kz', NULL, 'text', 10, 'Site'),
(16, 'site.map', 'Ссылка карта', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2905.778267900783!2d76.94888441499204!3d43.25606868603827!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836e923e9523ab%3A0x7d1f9c645c750e08!2z0YPQu9C40YbQsCDQktCw0LvQuNGF0LDQvdC-0LLQsCA2Mywg0JDQu9C80LDRgtGLIDA1MDAxMA!5e0!3m2!1sru!2skz!4v1585824606074!5m2!1sru!2skz', NULL, 'text_area', 11, 'Site'),
(17, 'site.youtube', 'Youtube', 'https://youtube.com', NULL, 'text', 12, 'Site'),
(18, 'site.instagram', 'Инстаграм', 'https://instagram.com', NULL, 'text', 13, 'Site'),
(19, 'site.facebook', 'Фейсбук', 'https://facebook.com', NULL, 'text', 14, 'Site');

-- --------------------------------------------------------

--
-- Структура таблицы `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Developer', 'demo@site.com', 'users/March2020/US7sGTomghPMGHLfeIxm.png', '$2y$10$L8DAdi1RjPFbkcL7eizfvOxZHZ4HKR4GfwJy6kQvhuSdbZc9Lep0.', 'CM6rJqPVMxqTU7mu767Gb1vMW2VYgNkHnfILScq3UgufP1TiLyM7Ky0ssTHz', '{\"locale\":\"ru\"}', '2020-03-26 22:41:05', '2020-03-30 11:33:32'),
(2, 4, 'Admin', 'your@mail.ru', 'users/default.png', '$2y$10$kQpfS1rmr04LGxSDN0qJfuNeGEXB6Zxa/mG1xdk0taUl4G.LYgrey', NULL, '{\"locale\":\"ru\"}', '2020-03-30 10:46:19', '2020-03-30 10:46:19');

-- --------------------------------------------------------

--
-- Структура таблицы `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `benefits`
--
ALTER TABLE `benefits`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Индексы таблицы `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Индексы таблицы `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Индексы таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Индексы таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Индексы таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Индексы таблицы `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Индексы таблицы `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `benefits`
--
ALTER TABLE `benefits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT для таблицы `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT для таблицы `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
