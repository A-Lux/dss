<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "menu_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $name
 * @property string|null $description
 * @property string|null $metaName
 * @property string|null $metaDesc
 * @property string|null $metaKey
 *
 * @property Menu $id0
 */
class MenuTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['description', 'metaDesc', 'metaKey'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['name', 'metaName'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'name' => 'Name',
            'description' => 'Description',
            'metaName' => 'Meta Name',
            'metaDesc' => 'Meta Desc',
            'metaKey' => 'Meta Key',
        ];
    }

    /**
     * Gets query for [[Id0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Menu::className(), ['id' => 'id']);
    }
}
