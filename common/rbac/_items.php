<?php
return [
    'guest' => [
        'type' => 1,
        'ruleName' => 'userRoleRule',
    ],
    'user' => [
        'type' => 1,
        'ruleName' => 'userRoleRule',
    ],
    'operator' => [
        'type' => 1,
        'ruleName' => 'userRoleRule',
        'children' => [
            'createOrders',
            'viewOrders',
            'indexOrders',
            'updateOrders',
            'deleteOrders',
            'createRequestSuggestion',
            'viewRequestSuggestion',
            'indexRequestSuggestion',
            'updateRequestSuggestion',
            'deleteRequestSuggestion',
        ],
    ],
    'manager' => [
        'type' => 1,
        'ruleName' => 'userRoleRule',
        'children' => [
            'operator',
            'createAbout',
            'viewAbout',
            'indexAbout',
            'updateAbout',
            'deleteAbout',
            'createBrand',
            'viewBrand',
            'indexBrand',
            'updateBrand',
            'deleteBrand',
            'viewContact',
            'indexContact',
            'updateContact',
            'createDelivery',
            'viewDelivery',
            'indexDelivery',
            'updateDelivery',
            'deleteDelivery',
            'createNews',
            'viewNews',
            'indexNews',
            'updateNews',
            'deleteNews',
            'createRequisites',
            'viewRequisites',
            'indexRequisites',
            'updateRequisites',
            'deleteRequisites',
            'createSuggestion',
            'viewSuggestion',
            'indexSuggestion',
            'updateSuggestion',
            'deleteSuggestion',
            'createBonusSystem',
            'viewBonusSystem',
            'indexBonusSystem',
            'updateBonusSystem',
            'deleteBonusSystem',
            'createDocuments',
            'viewDocuments',
            'indexDocuments',
            'updateDocuments',
            'deleteDocuments',
            'createGuarantee',
            'viewGuarantee',
            'indexGuarantee',
            'updateGuarantee',
            'deleteGuarantee',
            'createEmailForRequest',
            'viewEmailForRequest',
            'indexEmailForRequest',
            'updateEmailForRequest',
            'deleteEmailForRequest',
            'createFilterAttr',
            'viewFilterAttr',
            'indexFilterAttr',
            'updateFilterAttr',
            'deleteFilterAttr',
            'createFilterEntity',
            'viewFilterEntity',
            'indexFilterEntity',
            'updateFilterEntity',
            'deleteFilterEntity',
            'createFilterValue',
            'viewFilterValue',
            'indexFilterValue',
            'updateFilterValue',
            'deleteFilterValue',
            'createProduct',
            'viewProduct',
            'indexProduct',
            'updateProduct',
            'deleteProduct',
            'createCatalog',
            'viewCatalog',
            'indexCatalog',
            'updateCatalog',
            'deleteCatalog',
        ],
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userRoleRule',
        'children' => [
            'ownerAccess',
            'foreignAccess',
            'createUser',
            'viewUser',
            'indexUser',
            'updateUser',
            'createClient',
            'viewClient',
            'indexClient',
            'updateClient',
            'viewTranslation',
            'indexTranslation',
            'updateTranslation',
            'createMenu',
            'viewMenu',
            'indexMenu',
            'updateMenu',
            'createLanguage',
            'viewLanguage',
            'indexLanguage',
            'updateLanguage',
            'deleteLanguage',
            'createBanner',
            'viewBanner',
            'indexBanner',
            'updateBanner',
            'deleteBanner',
            'createLogo',
            'viewLogo',
            'indexLogo',
            'updateLogo',
            'deleteLogo',
            'createCity',
            'viewCity',
            'indexCity',
            'updateCity',
            'deleteCity',
            'manager',
        ],
    ],
    'developer' => [
        'type' => 1,
        'ruleName' => 'userRoleRule',
        'children' => [
            'deleteUser',
            'deleteMenu',
            'deleteTranslation',
            'createSourceMessageCategory',
            'viewSourceMessageCategory',
            'indexSourceMessageCategory',
            'updateSourceMessageCategory',
            'deleteSourceMessageCategory',
            'admin',
            'createContact',
            'deleteContact',
        ],
    ],
    'ownerAccess' => [
        'type' => 2,
        'description' => 'Доступ к своему контенту',
        'ruleName' => 'ownerAccess',
    ],
    'foreignAccess' => [
        'type' => 2,
        'description' => 'Доступ к чужому контенту',
        'ruleName' => 'foreignAccess',
    ],
    'createUser' => [
        'type' => 2,
        'description' => 'Создание пользователей',
    ],
    'viewUser' => [
        'type' => 2,
        'description' => 'Просмотр пользователей',
    ],
    'indexUser' => [
        'type' => 2,
        'description' => 'Листинг пользователей',
    ],
    'updateUser' => [
        'type' => 2,
        'description' => 'Изменение пользователей',
    ],
    'deleteUser' => [
        'type' => 2,
        'description' => 'Удаление пользователей',
    ],
    'createClient' => [
        'type' => 2,
        'description' => 'Создание информациях о клиенте',
    ],
    'viewClient' => [
        'type' => 2,
        'description' => 'Просмотр информациях о клиенте',
    ],
    'indexClient' => [
        'type' => 2,
        'description' => 'Листинг информациях о клиенте',
    ],
    'updateClient' => [
        'type' => 2,
        'description' => 'Изменение информациях о клиенте',
    ],
    'deleteClient' => [
        'type' => 2,
        'description' => 'Удаление информациях о клиенте',
    ],
    'createTranslation' => [
        'type' => 2,
        'description' => 'Создание переводов',
    ],
    'viewTranslation' => [
        'type' => 2,
        'description' => 'Просмотр переводов',
    ],
    'indexTranslation' => [
        'type' => 2,
        'description' => 'Листинг переводов',
    ],
    'updateTranslation' => [
        'type' => 2,
        'description' => 'Изменение переводов',
    ],
    'deleteTranslation' => [
        'type' => 2,
        'description' => 'Удаление переводов',
    ],
    'createSourceMessageCategory' => [
        'type' => 2,
        'description' => 'Создание описаний категорий переводов',
    ],
    'viewSourceMessageCategory' => [
        'type' => 2,
        'description' => 'Просмотр описаний категорий переводов',
    ],
    'indexSourceMessageCategory' => [
        'type' => 2,
        'description' => 'Листинг описаний категорий переводов',
    ],
    'updateSourceMessageCategory' => [
        'type' => 2,
        'description' => 'Изменение описаний категорий переводов',
    ],
    'deleteSourceMessageCategory' => [
        'type' => 2,
        'description' => 'Удаление описаний категорий переводов',
    ],
    'createMenu' => [
        'type' => 2,
        'description' => 'Создание пунктов меню',
    ],
    'viewMenu' => [
        'type' => 2,
        'description' => 'Просмотр пунктов меню',
    ],
    'indexMenu' => [
        'type' => 2,
        'description' => 'Листинг пунктов меню',
    ],
    'updateMenu' => [
        'type' => 2,
        'description' => 'Изменение пунктов меню',
    ],
    'deleteMenu' => [
        'type' => 2,
        'description' => 'Удаление пунктов меню',
    ],
    'createLanguage' => [
        'type' => 2,
        'description' => 'Создание языка',
    ],
    'viewLanguage' => [
        'type' => 2,
        'description' => 'Просмотр языка',
    ],
    'indexLanguage' => [
        'type' => 2,
        'description' => 'Листинг языка',
    ],
    'updateLanguage' => [
        'type' => 2,
        'description' => 'Изменение языка',
    ],
    'deleteLanguage' => [
        'type' => 2,
        'description' => 'Удаление языка',
    ],
    'createBanner' => [
        'type' => 2,
        'description' => 'Создание баннерами',
    ],
    'viewBanner' => [
        'type' => 2,
        'description' => 'Просмотр баннерами',
    ],
    'indexBanner' => [
        'type' => 2,
        'description' => 'Листинг баннерами',
    ],
    'updateBanner' => [
        'type' => 2,
        'description' => 'Изменение баннерами',
    ],
    'deleteBanner' => [
        'type' => 2,
        'description' => 'Удаление баннерами',
    ],
    'createLogo' => [
        'type' => 2,
        'description' => 'Создание логотипа',
    ],
    'viewLogo' => [
        'type' => 2,
        'description' => 'Просмотр логотипа',
    ],
    'indexLogo' => [
        'type' => 2,
        'description' => 'Листинг логотипа',
    ],
    'updateLogo' => [
        'type' => 2,
        'description' => 'Изменение логотипа',
    ],
    'deleteLogo' => [
        'type' => 2,
        'description' => 'Удаление логотипа',
    ],
    'createCity' => [
        'type' => 2,
        'description' => 'Создание города',
    ],
    'viewCity' => [
        'type' => 2,
        'description' => 'Просмотр города',
    ],
    'indexCity' => [
        'type' => 2,
        'description' => 'Листинг города',
    ],
    'updateCity' => [
        'type' => 2,
        'description' => 'Изменение города',
    ],
    'deleteCity' => [
        'type' => 2,
        'description' => 'Удаление города',
    ],
    'createAbout' => [
        'type' => 2,
        'description' => 'Создание о компании',
    ],
    'viewAbout' => [
        'type' => 2,
        'description' => 'Просмотр о компании',
    ],
    'indexAbout' => [
        'type' => 2,
        'description' => 'Листинг о компании',
    ],
    'updateAbout' => [
        'type' => 2,
        'description' => 'Изменение о компании',
    ],
    'deleteAbout' => [
        'type' => 2,
        'description' => 'Удаление о компании',
    ],
    'createBrand' => [
        'type' => 2,
        'description' => 'Создание бренда',
    ],
    'viewBrand' => [
        'type' => 2,
        'description' => 'Просмотр бренда',
    ],
    'indexBrand' => [
        'type' => 2,
        'description' => 'Листинг бренда',
    ],
    'updateBrand' => [
        'type' => 2,
        'description' => 'Изменение бренда',
    ],
    'deleteBrand' => [
        'type' => 2,
        'description' => 'Удаление бренда',
    ],
    'createContact' => [
        'type' => 2,
        'description' => 'Создание контакта',
    ],
    'viewContact' => [
        'type' => 2,
        'description' => 'Просмотр контакта',
    ],
    'indexContact' => [
        'type' => 2,
        'description' => 'Листинг контакта',
    ],
    'updateContact' => [
        'type' => 2,
        'description' => 'Изменение контакта',
    ],
    'deleteContact' => [
        'type' => 2,
        'description' => 'Удаление контакта',
    ],
    'createDelivery' => [
        'type' => 2,
        'description' => 'Создание страницы оплата и доставка',
    ],
    'viewDelivery' => [
        'type' => 2,
        'description' => 'Просмотр страницы оплата и доставка',
    ],
    'indexDelivery' => [
        'type' => 2,
        'description' => 'Листинг страницы оплата и доставка',
    ],
    'updateDelivery' => [
        'type' => 2,
        'description' => 'Изменение страницы оплата и доставка',
    ],
    'deleteDelivery' => [
        'type' => 2,
        'description' => 'Удаление страницы оплата и доставка',
    ],
    'createNews' => [
        'type' => 2,
        'description' => 'Создание новости',
    ],
    'viewNews' => [
        'type' => 2,
        'description' => 'Просмотр новости',
    ],
    'indexNews' => [
        'type' => 2,
        'description' => 'Листинг новости',
    ],
    'updateNews' => [
        'type' => 2,
        'description' => 'Изменение новости',
    ],
    'deleteNews' => [
        'type' => 2,
        'description' => 'Удаление новости',
    ],
    'createRequisites' => [
        'type' => 2,
        'description' => 'Создание реквизита',
    ],
    'viewRequisites' => [
        'type' => 2,
        'description' => 'Просмотр реквизита',
    ],
    'indexRequisites' => [
        'type' => 2,
        'description' => 'Листинг реквизита',
    ],
    'updateRequisites' => [
        'type' => 2,
        'description' => 'Изменение реквизита',
    ],
    'deleteRequisites' => [
        'type' => 2,
        'description' => 'Удаление реквизита',
    ],
    'createSuggestion' => [
        'type' => 2,
        'description' => 'Создание страницы жалобы и предложение',
    ],
    'viewSuggestion' => [
        'type' => 2,
        'description' => 'Просмотр страницы жалобы и предложение',
    ],
    'indexSuggestion' => [
        'type' => 2,
        'description' => 'Листинг страницы жалобы и предложение',
    ],
    'updateSuggestion' => [
        'type' => 2,
        'description' => 'Изменение страницы жалобы и предложение',
    ],
    'deleteSuggestion' => [
        'type' => 2,
        'description' => 'Удаление страницы жалобы и предложение',
    ],
    'createBonusSystem' => [
        'type' => 2,
        'description' => 'Создание бонусной системой',
    ],
    'viewBonusSystem' => [
        'type' => 2,
        'description' => 'Просмотр бонусной системой',
    ],
    'indexBonusSystem' => [
        'type' => 2,
        'description' => 'Листинг бонусной системой',
    ],
    'updateBonusSystem' => [
        'type' => 2,
        'description' => 'Изменение бонусной системой',
    ],
    'deleteBonusSystem' => [
        'type' => 2,
        'description' => 'Удаление бонусной системой',
    ],
    'createDocuments' => [
        'type' => 2,
        'description' => 'Создание документа',
    ],
    'viewDocuments' => [
        'type' => 2,
        'description' => 'Просмотр документа',
    ],
    'indexDocuments' => [
        'type' => 2,
        'description' => 'Листинг документа',
    ],
    'updateDocuments' => [
        'type' => 2,
        'description' => 'Изменение документа',
    ],
    'deleteDocuments' => [
        'type' => 2,
        'description' => 'Удаление документа',
    ],
    'createGuarantee' => [
        'type' => 2,
        'description' => 'Создание страницы гарантия и возврат',
    ],
    'viewGuarantee' => [
        'type' => 2,
        'description' => 'Просмотр страницы гарантия и возврат',
    ],
    'indexGuarantee' => [
        'type' => 2,
        'description' => 'Листинг страницы гарантия и возврат',
    ],
    'updateGuarantee' => [
        'type' => 2,
        'description' => 'Изменение страницы гарантия и возврат',
    ],
    'deleteGuarantee' => [
        'type' => 2,
        'description' => 'Удаление страницы гарантия и возврат',
    ],
    'createEmailForRequest' => [
        'type' => 2,
        'description' => 'Создание электронной почтой для обратной связи',
    ],
    'viewEmailForRequest' => [
        'type' => 2,
        'description' => 'Просмотр электронной почтой для обратной связи',
    ],
    'indexEmailForRequest' => [
        'type' => 2,
        'description' => 'Листинг электронной почтой для обратной связи',
    ],
    'updateEmailForRequest' => [
        'type' => 2,
        'description' => 'Изменение электронной почтой для обратной связи',
    ],
    'deleteEmailForRequest' => [
        'type' => 2,
        'description' => 'Удаление электронной почтой для обратной связи',
    ],
    'createRequestSuggestion' => [
        'type' => 2,
        'description' => 'Создание обратной связи',
    ],
    'viewRequestSuggestion' => [
        'type' => 2,
        'description' => 'Просмотр обратной связи',
    ],
    'indexRequestSuggestion' => [
        'type' => 2,
        'description' => 'Листинг обратной связи',
    ],
    'updateRequestSuggestion' => [
        'type' => 2,
        'description' => 'Изменение обратной связи',
    ],
    'deleteRequestSuggestion' => [
        'type' => 2,
        'description' => 'Удаление обратной связи',
    ],
    'createOrders' => [
        'type' => 2,
        'description' => 'Создание заказами',
    ],
    'viewOrders' => [
        'type' => 2,
        'description' => 'Просмотр заказами',
    ],
    'indexOrders' => [
        'type' => 2,
        'description' => 'Листинг заказами',
    ],
    'updateOrders' => [
        'type' => 2,
        'description' => 'Изменение заказами',
    ],
    'deleteOrders' => [
        'type' => 2,
        'description' => 'Удаление заказами',
    ],
    'createFilterAttr' => [
        'type' => 2,
        'description' => 'Создание аттрибутами фильтров',
    ],
    'viewFilterAttr' => [
        'type' => 2,
        'description' => 'Просмотр аттрибутами фильтров',
    ],
    'indexFilterAttr' => [
        'type' => 2,
        'description' => 'Листинг аттрибутами фильтров',
    ],
    'updateFilterAttr' => [
        'type' => 2,
        'description' => 'Изменение аттрибутами фильтров',
    ],
    'deleteFilterAttr' => [
        'type' => 2,
        'description' => 'Удаление аттрибутами фильтров',
    ],
    'createFilterEntity' => [
        'type' => 2,
        'description' => 'Создание сущностями фильтра',
    ],
    'viewFilterEntity' => [
        'type' => 2,
        'description' => 'Просмотр сущностями фильтра',
    ],
    'indexFilterEntity' => [
        'type' => 2,
        'description' => 'Листинг сущностями фильтра',
    ],
    'updateFilterEntity' => [
        'type' => 2,
        'description' => 'Изменение сущностями фильтра',
    ],
    'deleteFilterEntity' => [
        'type' => 2,
        'description' => 'Удаление сущностями фильтра',
    ],
    'createFilterValue' => [
        'type' => 2,
        'description' => 'Создание значение фильтров',
    ],
    'viewFilterValue' => [
        'type' => 2,
        'description' => 'Просмотр значение фильтров',
    ],
    'indexFilterValue' => [
        'type' => 2,
        'description' => 'Листинг значение фильтров',
    ],
    'updateFilterValue' => [
        'type' => 2,
        'description' => 'Изменение значение фильтров',
    ],
    'deleteFilterValue' => [
        'type' => 2,
        'description' => 'Удаление значение фильтров',
    ],
    'createProduct' => [
        'type' => 2,
        'description' => 'Создание продуктами',
    ],
    'viewProduct' => [
        'type' => 2,
        'description' => 'Просмотр продуктами',
    ],
    'indexProduct' => [
        'type' => 2,
        'description' => 'Листинг продуктами',
    ],
    'updateProduct' => [
        'type' => 2,
        'description' => 'Изменение продуктами',
    ],
    'deleteProduct' => [
        'type' => 2,
        'description' => 'Удаление продуктами',
    ],
    'createCatalog' => [
        'type' => 2,
        'description' => 'Создание каталогом',
    ],
    'viewCatalog' => [
        'type' => 2,
        'description' => 'Просмотр каталогом',
    ],
    'indexCatalog' => [
        'type' => 2,
        'description' => 'Листинг каталогом',
    ],
    'updateCatalog' => [
        'type' => 2,
        'description' => 'Изменение каталогом',
    ],
    'deleteCatalog' => [
        'type' => 2,
        'description' => 'Удаление каталогом',
    ],
];
