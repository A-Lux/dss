<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%contact}}`.
 */
class m200121_113927_create_contact_table extends Migration
{
    public $table               = 'contact';
    public $translationTable    = 'contact_translation';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'address'                   => $this->text()->null(),
            'email'                     => $this->string(255)->null(),
            'phone'                     => $this->string(255)->null(),
            'schedule'                  => $this->string(255)->null(),
            'iframe'                    => $this->text()->null(),
            'created_at'                => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $this->createTable("{{{$this->translationTable}}}", [
            'id'                        => $this->integer()->notNull(),
            'language'                  => $this->string(16)->notNull(),
            'address'                   => $this->string(255)->null(),
            'schedule'                  => $this->string(255)->null(),
        ], $tableOptions);

        $this->addPrimaryKey("pk_{$this->translationTable}_id_language", "{{{$this->translationTable}}}", ['id', 'language']);
        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->translationTable}_{$this->table}", "{{{$this->translationTable}}}", 'id', "{{{$this->table}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->createIndex("idx_{$this->translationTable}_language", "{{{$this->translationTable}}}", 'language');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->translationTable}_{$this->table}", "{{{$this->translationTable}}}");
        $this->dropTable("{{{$this->translationTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
