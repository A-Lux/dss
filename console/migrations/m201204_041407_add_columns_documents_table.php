<?php

use yii\db\Migration;

/**
 * Class m201204_041407_add_columns_documents_table
 */
class m201204_041407_add_columns_documents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('documents', 'subcategory_id', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('documents', 'subcategory_id');
    }
}
