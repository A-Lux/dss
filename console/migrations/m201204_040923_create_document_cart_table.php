<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%document_cart}}`.
 */
class m201204_040923_create_document_cart_table extends Migration
{
    public $table               = 'document_cart';
    public $translationTable    = 'document_cart_translation';
    public $categoryTable       = 'category_documents';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'category_id'       => $this->integer()->null(),
            'name'              => $this->string(255)->null(),
            'description'       => $this->text()->null(),
            'phone'             => $this->string(255)->null(),
            'image'             => $this->text()->null(),
            'sort'              => $this->integer()->null(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $this->createTable("{{{$this->translationTable}}}", [
            'id'                => $this->integer()->notNull(),
            'language'          => $this->string(16)->notNull(),
            'name'              => $this->string(255)->null(),
            'description'       => $this->text()->null(),
        ], $tableOptions);

        $this->addPrimaryKey("pk_{$this->translationTable}_id_language",
            "{{{$this->translationTable}}}", ['id', 'language']);
        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->translationTable}_{$this->table}",
            "{{{$this->translationTable}}}", 'id', "{{{$this->table}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->createIndex("idx_{$this->translationTable}_language",
            "{{{$this->translationTable}}}", 'language');

        $this->addForeignKey("fk_{$this->table}_{$this->categoryTable}",
            "{{{$this->table}}}", 'category_id',
            "{{{$this->categoryTable}}}", 'id',
            'CASCADE', $onUpdateConstraint);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->categoryTable}",
            "{{{$this->categoryTable}}}");

        $this->dropForeignKey("fk_{$this->translationTable}_{$this->table}",
            "{{{$this->translationTable}}}");
        $this->dropTable("{{{$this->translationTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
