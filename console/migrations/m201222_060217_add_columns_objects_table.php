<?php

use yii\db\Migration;

/**
 * Class m201222_060217_add_columns_objects_table
 */
class m201222_060217_add_columns_objects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('objects', 'status', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('objects', 'status');
    }
}
