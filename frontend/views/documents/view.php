<?php

/* @var $this View */
/* @var $subcategory SubcategoryDocuments */
/* @var $documents Documents */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\SubcategoryDocuments;
use common\models\Documents;

?>

<div class="container">

    <? foreach ($subcategory as $item): ?>
        <div class="finans-title">
            <?= $item->name; ?>
        </div>
        <div class="block-finans">
            <? foreach ($documents as $document): ?>
                <? if($item->id == $document->subcategory_id): ?>
                    <a href="<?= $document->getFile(); ?>" target="_blank">
                        <span><img src="/images/icon/docum.svg" alt=""></span>
                        <?= $document->name; ?>
                    </a>
                <? endif; ?>
            <? endforeach; ?>
        </div>
    <? endforeach; ?>

</div>
