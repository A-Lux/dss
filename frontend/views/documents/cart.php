<?php

/* @var $this View */
/* @var $carts DocumentCart */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\DocumentCart;

?>

<div class="container">
    <div class="rent-title"><?= Yii::t('app','rent') ?></div>
    <div class="block-rent">

        <? foreach ($carts as $cart): ?>
            <div class="item-rent-main">
                <div class="item-rent">
                    <div class="item-img">
                        <img src="<?= $cart->getImage() ?>" alt="">
                    </div>
                    <div class="item-title-rent">
                        <?= $cart->name; ?>
                    </div>
                    <div class="item-text-name-rent">
                        <?= $cart->description; ?>
                    </div>
                    <div class="item-btn-name-rent">
                        <a href="tel:<?= $cart->phone; ?>">Позвонить</a>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
</div>
