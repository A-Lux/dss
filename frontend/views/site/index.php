<?php

/* @var $advantages Advantages */
/* @var $about About */
/* @var $gallery Gallery */
/* @var $objects Objects */
/* @var $team Team */
/* @var $symbols Symbols */
/* @var $menu Menu */
/* @var $categoryDocuments CategoryDocuments */


use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\About;
use common\models\Advantages;
use common\models\Gallery;
use common\models\Objects;
use common\models\Team;
use common\models\Symbols;
use common\models\Menu;
use common\models\CategoryDocuments;

?>

<div class="content company-color">
	<div class="my-container">
		<div class="company">
			<div class="company-project">
				<? foreach ($advantages as $advantage): ?>
				<div class="company-project-<?= $advantage->id == 1 ? 'first' : 'second' ?>-block">
					<p><?= $advantage->title; ?></p>
					<p><?= $advantage->content; ?></p>
				</div>
				<? endforeach; ?>
			</div>
			<div id="about" class="company-about">
				<div class="company-title">
					<?= ArrayHelper::getValue($menu, 'about')->name; ?>
				</div>
				<div class="company-subtitle">
					<?= $about ? $about->title : ' '; ?>
				</div>
				<div class="company-text">
					<?= $about ? $about->content : ' '; ?>
				</div>
			</div>
		</div>
		<div id="gallery" class="gallery">
			<div class="gallery-title">
				<?= ArrayHelper::getValue($menu, 'gallery')->name; ?>
			</div>
			<div class="gallery-subtitle">
				<?= ArrayHelper::getValue($menu, 'gallery')->description; ?>
			</div>
			<div class="gallery-pictures">
				<div class="gallery-carousel">

					<? 	
						$i = 0;
						$j = 0;
						$count = count($gallery);

						foreach ($gallery as $value): ?>
					<?
						if(($j + 1) == $i) {
							$j = $j + 1;
							echo('</div>');
							echo('</div>');
							}
							if($i == 0 || ($i % 1) == 0) {
								echo('<div class="gallery-slider">');
								echo('<div class="slider-horizontal">');
							}
							$i = $i + 1;
							?>

					<div class="slider-item">
						<img src="<?= '/backend/web/uploads/images/gallery/' . $value['image'] ?>" alt="">
					</div>


					<?if($count == $i) {
						echo('</div>');
						echo('</div>');
						}
						?>
					<? endforeach; ?>
				</div>
				<div class="gallery-video">
					<iframe width="589" height="459" src="/backend/web/uploads/files/video/DSS.mp4" frameborder="0"
						allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
						allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content staff-color">
	<div class="my-container">
		<div class="staff">
			<div class="staff-title">
				<?= ArrayHelper::getValue($menu, 'team')->name; ?>
			</div>
			<div class="staff-subtitle">
				<?= ArrayHelper::getValue($menu, 'team')->description; ?>
			</div>
			<div class="staff-carousel">
				<? foreach ($team as $item): ?>
				<div data-staffopen class="item-carousel <?= $item->id == 1 ? 'item-dir' : ' ' ?>" id="<?= $item->id; ?>">
					<img class='upimg' src="<?= $item->getImage(); ?>" alt="">
					<p class="<?= $item->id == 1 ? 'dir' : 'manager-stf' ?>">
						<?= $item->position; ?>
					</p>
					<p><?= $item->name; ?></p>
					<input class="setdirector" type="hidden" value="<?= $item->position; ?>">
					<input class="setname" type="hidden" value="<?= $item->name; ?>">
					<input class="setcontent" type="hidden" value='<?= $item->content; ?>' width="0">
				</div>
				<? endforeach; ?>
			</div>
		</div>
	</div>
</div>
<div id="documents" class="conten document-color">
	<div class="my-container">
		<div class="document">
			<div class="documen-title">
				<?= ArrayHelper::getValue($menu, 'documents')->name; ?>
			</div>
			<div class="documen-subtitle">
				<?= ArrayHelper::getValue($menu, 'documents')->description; ?></div>
			<div class="type-documen">
				<? foreach ($categoryDocuments as $categoryDocument): ?>

				<? if($categoryDocument->status == 1): ?>
				<a href="<?= Url::to(['/documents/view', 'id' => $categoryDocument->id]); ?>">
					<div class="item-documen">
						<?= $categoryDocument->name; ?>
					</div>
				</a>
				<? elseif($categoryDocument->status == 2): ?>
				<a href="<?= Url::to(['/documents/cart', 'id' => $categoryDocument->id]); ?>">
					<div class="item-documen">
						<?= $categoryDocument->name; ?>
					</div>
				</a>
				<? else: ?>
				<a href="<?= $categoryDocument->fileDocument; ?>" target="_blank">
					<div class="item-documen">
						<?= $categoryDocument->name; ?>
					</div>
				</a>
				<? endif; ?>
				<? endforeach; ?>

			</div>
		</div>
	</div>
</div>
<div id="objects" class="content object-color">
	<div class="fake-bg"></div>
	<div class="my-container">
		<div class="our-object">
			<div class="object-title">
				<?= ArrayHelper::getValue($menu, 'objects')->name; ?>
			</div>
			<div class="object-subtitle">
				<?= ArrayHelper::getValue($menu, 'objects')->description; ?>
			</div>
			<div class="pictures-object">
				<? foreach ($objects as $object): ?>
				<div class="pictures-object-horizontal">
					<? foreach ($object as $value): ?>
					<div class="pictures-object-item item-line-object" id="object-<?= $value->id; ?>">
						<img src="<?= $value->getImage(); ?>" alt="">
						<p><?= $value->title; ?></p>
						<input class="setid" type="hidden" value="object-<?= $value->id; ?>">
						<input class="settitle" type="hidden" value='<?= $value->title; ?>'>
						<input class="settext" type="hidden" value='<?= $value->content; ?>'>
					</div>
					<? endforeach; ?>
				</div>
				<? endforeach; ?>


				<!--				<div class="pictures-object-horizontal">-->
				<!--					<div class="pictures-object-item item-line-object">-->
				<!--						<img src="/images/pictures/city-3.png" alt="">-->
				<!--						<p>ВСК «Медеу»</p>-->
				<!--					</div>-->
				<!--					<div class="pictures-object-item">-->
				<!--						<img src="/images/pictures/city-4.png" alt="">-->
				<!--						<p>Центральный стадион</p>-->
				<!--					</div>-->
				<!--				</div>-->
			</div>
		</div>
	</div>
</div>
<div class="content vert-line">
	<div class="my-container">
		<div class="symbol-city">
			<div class="symbol-title">
				<?= ArrayHelper::getValue($menu, 'symbols')->name; ?>
			</div>
			<div class="symbol-subtitle">
				<?= ArrayHelper::getValue($menu, 'symbols')->description; ?>
			</div>
			<? foreach ($symbols as $symbol): ?>
			<div class="symbol-block">
				<div class="symbol-our">
					<div class="pict-symbol">
						<img src="<?= $symbol->getImage(); ?>" alt="">
					</div>
					<div class="text-symbol">
						<div class="text-symbol-name">
							<?= $symbol->title; ?>
						</div>
						<div class="text-symbol-description">
							<?= $symbol->content; ?>
						</div>
					</div>
				</div>
				<div class="symbol-more-inf">
					<a href="<?= $symbol->link; ?>" target="_blank">
						<?= Yii::t('app','read') ?>
					</a>
				</div>
			</div>
			<? endforeach; ?>
		</div>
	</div>
</div>

<div class="overlay-window hide">
	<div class="feedback-modal-window">
		<div class="item-modal">
			<div class="close-input">
				<div data-close class="closebtn">&times;</div>
			</div>
			<div class="modal-main">
				<div class="name"><?= Yii::t('app','contact') ?></div>
			</div>
			<form>
				<div class="form-feedback">
					<input tabindex="1" placeholder="<?= Yii::t('app','name') ?>" type="text">
					<input tabindex="2" placeholder="<?= Yii::t('app','phone') ?>" type="tel">
					<input tabindex="3" placeholder="Email" type="email">
					<textarea tabindex="3" placeholder=""></textarea>
					<button type="submit"><?= Yii::t('app','send_message') ?></button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="overlay-slider-window hide-staff">
	<div class="paragraph-modal">
		<div class="modal-close-slider">
			<div class="close-input">
				<div data-staffclose class="closebtn">&times;</div>
			</div>
		</div>
		<div class="title-slider-modal">
			<p class="director">Директор</p>
			<p class="subparagraph">Нуров Наиль</p>
		</div>
		<div class="text-slider-modal">
			<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro explicabo perferendis vero quod ab
				ipsa
				saepe libero deserunt ducimus sunt enim illo culpa necessitatibus adipisci, ut sint nulla error
				inventore.
				Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro explicabo perferendis vero quod ab
				ipsa
				saepe libero deserunt ducimus sunt enim illo culpa necessitatibus adipisci, ut sint nulla error
				inventore.
				Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro explicabo perferendis vero quod ab
				ipsa
				saepe libero deserunt ducimus sunt enim illo culpa necessitatibus adipisci, ut sint nulla error
				inventore.
				Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro explicabo perferendis vero quod ab
				ipsa
				saepe libero deserunt ducimus sunt enim illo culpa necessitatibus adipisci, ut sint nulla error
				inventore.
			</p>
		</div>
	</div>
</div>

<div class="overlay-object-window hide-object">
	<div class="paragraph-modal">
		<div class="modal-close-slider">
			<div class="close-input">
				<div class="closebtn closeobject">&times;</div>
			</div>
		</div>
		<div class="title-slider-modal">
			<p class="object-name">АТЛЕТИЧЕСКАЯ ДЕРЕВНЯ</p>
		</div>
		<div>
			<p class="text-slider-modal object-item-modal">Lorem ipsum dolor sit, amet consectetur adipisicing elit.
				Porro
				explicabo perferendis vero quod ab ipsa
				saepe libero deserunt ducimus sunt enim illo culpa necessitatibus adipisci, ut sint nulla error
				inventore.
				Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro explicabo perferendis vero quod ab
				ipsa
				saepe libero deserunt ducimus sunt enim illo culpa necessitatibus adipisci, ut sint nulla error
				inventore.
				Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro explicabo perferendis vero quod ab
				ipsa
				saepe libero deserunt ducimus sunt enim illo culpa necessitatibus adipisci, ut sint nulla error
				inventore.
				Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro explicabo perferendis vero quod ab
				ipsa
				saepe libero deserunt ducimus sunt enim illo culpa necessitatibus adipisci, ut sint nulla error
				inventore.
			</p>
		</div>
	</div>
</div>