<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\ToastrAlert;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="/css/style.css">
	 <link href="https://fonts.googleapis.com/css2?family=Bitter:wght@700&display=swap" rel="stylesheet">
	 <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@400;500;600;700&display=swap" rel="stylesheet">
    <script src="/js/jquery-3.5.1.js"></script>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!-- HEADER -->
<?= $this->render('_header') ?>
<!-- END HEADER -->

<?= \yii2mod\alert\Alert::widget() ?>
<!-- CONTENT -->
    <?= $content ?>
<!-- END CONTENT -->

<!-- FOOTER -->
<?= $this->render('_footer') ?>
<!-- END FOOTER -->

<script src="/js/slick.min.js"></script>
<script src="/js/app.js"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
