<?php

use yii\helpers\ArrayHelper;

$menu       = \Yii::$app->view->params['menuHeader'];
$banner     = \Yii::$app->view->params['banner'];
$logo       = \Yii::$app->view->params['logoHeader'];
$object     = \Yii::$app->view->params['object'];
$contact    = \Yii::$app->view->params['contacts'];
$socialNetworks = \Yii::$app->view->params['socialNetworks'];

?>

<footer id="contact" class="footer">
	<div class="content">
		<div class="my-container">
			<div class="footer-column">
			
				<div class="column-item">
				<div class="name" style="padding-top:80px;"><?= Yii::t('app','contacts') ?></div>
				
					<!-- <p>У вас есть вопросы
						и не хотите с нами связаться?
						<span>Позвоните или посетите нас.</span></p> -->
					<p><span><img src="/images/icon/icon-phone.png" alt=""></span>
						<span><a
								href="tel:<?= $contact ? $contact->phone : ' ' ?>"><?= $contact ? $contact->phone : ' ' ?></a>
						</span>
					</p>
					<p style="font-size:18px; color:#fff"> <?= $contact ? $contact->address : ' ' ?>
					</p>
				</div>
				<div class="column-item-two">
					<!-- <p>Вы хотите получить подробное
						предложение работы?
						<span>Используйте нашу онлайн-форму.</span></p> -->
						<div class="name" style="padding-top:80px;"><?= Yii::t('app','contact') ?></div>
					<p data-open><a href="#"><?= Yii::t('app','send_message') ?></a></p>
					<!-- <p>с вами свяжуться в течении
						1-2х часов
					</p> -->
				</div>
				<div class="column-item-three">
					<p>
						<? foreach ($socialNetworks as $socialNetwork): ?>
						<span>
                            <a href="<?= $socialNetwork->link; ?>" target="_blank">
							<img src="<?= $socialNetwork->getImage(); ?>" alt="">
                            </a>
						</span>
						<? endforeach; ?>
					</p>
					<p>E-mail: <?= $contact ? $contact->email : ' ' ?></p>
					<p> <?= Yii::t('app','schedule') ?> : <br> <?= $contact ? $contact->schedule : ' ' ?></p>
				</div>
			</div>
		</div>
		<div class="footer-menu">
			<ul>
				<? foreach ($menu as $value): ?>
				<li>
					<a href="<?= $value->url; ?>">
						<?= $value->name; ?>
					</a>
				</li>
				<? endforeach; ?>
			</ul>
		</div>
		<div class="container-for-text-and-icon-developer-studio">
			<div class="rights-menu"><?= Yii::t('app','copyright') ?></div>
			<!-- <div class="icon-studio"><span>Разработано в </span><a href="https://www.a-lux.kz/"><img
						src="/frontend/web/images/icon/icon-developer.svg" alt=""></a></div>
		</div> -->
		</div>
</footer>