<?php

use yii\helpers\Url;

$menu       = \Yii::$app->view->params['menuHeader'];
$banner     = \Yii::$app->view->params['banner'];
$logo       = \Yii::$app->view->params['logoHeader'];
$object     = \Yii::$app->view->params['object'];
$contact    = \Yii::$app->view->params['contacts'];
$socialNetworks = \Yii::$app->view->params['socialNetworks'];
$languages      = \Yii::$app->view->params['language'];

?>

<header class="header">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 px-0 d-flex back-dark-gray">
				<div class="logo">
					<a href="/">
						<img src="<?= $logo->getImage(); ?>" alt="">
					</a>
				</div>
				<div class="nav-menu">
					<ul class="list-unstyled d-flex justify-content-between">
						<? foreach ($menu as $value): ?>
						<li>
							<a href="<?= $value->url; ?>">
								<?= $value->name; ?>
							</a>
						</li>
						<? endforeach; ?>
					</ul>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 px-0 d-flex justify-content-start dpnone back-dark-gray">
				<div data-open class="feedback-connection dpoff">
<!--					<p>ОБРАТНАЯ СВЯЗЬ</p>-->
					<p><?= Yii::t('app','contact') ?></p>
				</div>
				<div class="language dpoff">
                    <? foreach ($languages as $language): ?>
                        <a href="<?= Url::to(array_merge(\Yii::$app->request->get(),
                            [\Yii::$app->controller->route, 'language' => $language->code])) ?>"
                           class="<?= $language->code == Yii::$app->language ? 'lang-active' : '' ?>"
                        >
                            <?= $language->name; ?>
                        </a>
                    <? endforeach; ?>
				</div>
				<div data-burg class="header__burger">
					<span></span>
				</div>
				<div data-menu class="burger__menu hide">
					<div class="burger__list">
						<ul style="list-style: none;">
							<? foreach ($menu as $value): ?>
							<li style="margin-bottom: 1rem;">
								<a href="<?= $value->url; ?>" class="burger__link">
									<?= $value->name; ?>
								</a>
							</li>
							<? endforeach; ?>
						</ul>
						<div data-brg class="feedback-burger">
<!--							<p>Обратная Связь</p>-->
                            <p><?= Yii::t('app','contact') ?></p>
						</div>
						<div class="language-burger">
                            <? foreach ($languages as $language): ?>
                                <a href="<?= Url::to(array_merge(\Yii::$app->request->get(),
                                    [\Yii::$app->controller->route, 'language' => $language->code])) ?>"
                                   class="<?= $language->code == Yii::$app->language ? 'lang-active' : '' ?>"
                                >
                                    <?= $language->name; ?>
                                </a>
                            <? endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
        <? if(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index'): ?>
            <div class="row">
                <div class="col-xl-8 col-lg-8  col-md-8 px-0 back-dark-gray">
                    <div class="header-inform-block">
                        <div class="header-text">
                            <?= $banner ? $banner->content : ' ' ?>
                        </div>
                        <div class="header-title">
                            <?= $banner ? $banner->title : ' ' ?>
                        </div>
                        <div class="pictur-header-block dp-big-screen">
                            <img src="<?= $object ? $object->getImage() : ' ' ?>" alt="">
                        </div>
                        <div class="solid-header-black dp-big-screen">
                            <p><?= $object ? $object->title : ' ' ?></p>
                            <?= $object ? substr($object->content, 0, 120) : ' ' ?>
                            <p><a href="https://medey.info/" target="_blank"><?= Yii::t('app','details') ?></a> </p>
                        </div>
                        <div class="solid-small-header-black dp-big-screen">
<!--                            <p>наши объекты</p>-->
                            <?= Yii::t('app','objects') ?>
                        </div>
                        <div class="header-button-group">
                            <button><a href="#about"><?= Yii::t('app','about') ?></a></button>
                            <button><a href="#gallery">Галерея</a></button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 px-0 company-color dpnone">
                    <div class="pictur-header-block">
                        <img src="<?= $object ? $object->getImage() : ' ' ?>" alt="">
                    </div>
                    <div class="solid-header-black">
                        <p><?= $object ? $object->title : ' ' ?></p>
                        <?= $object ? substr($object->content, 0, 120) : ' ' ?>
                        <p><a href="https://medey.info/" target="_blank"><?= Yii::t('app','details') ?></a></p>
                    </div>
                    <div class="solid-small-header-black">
                        <p><a href="#objects"><?= Yii::t('app','objects') ?></a></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-8 px-0 back-dark-gray">
                    <div class="header-tel-block">
                        <span><img src="/images/icon/icon-phone.png" alt=""></span>
                        <span><a href="tel:<?= $contact ? $contact->phone : ' ' ?>"><?= $contact ? $contact->phone : ' ' ?></a>
                        </span>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-4 px-0 company-color">
                    <div class="social">
                        <? foreach ($socialNetworks as $socialNetwork): ?>
                        <a href="<?= $socialNetwork->link; ?>" target="_blank">
                            <div class="block-inst">
                                <img src="<?= $socialNetwork->getImage(); ?>"  alt="">
                            </div>
                        </a>
                        <? endforeach; ?>

                    </div>
                </div>
            </div>

        <? endif; ?>
	</div>
</header>