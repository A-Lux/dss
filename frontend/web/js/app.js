$(document).ready(function(){
	$('.gallery-carousel').slick({
		arrows: true,
      prevArrow: '<div id="slider-1" class="slick-prev"></div>',
		nextArrow: '<div id="slider-1" class="slick-next"></div>',
	});
 });


$('.staff-carousel').slick({
	centerMode: true,
	centerPadding: '1px',
	slidesToShow: 5,
	prevArrow: '<div id="slider-2" class="slick-prev"></div>',
	nextArrow: '<div id="slider-2" class="slick-next"></div>',
	mobileFirst: true,
	responsive: [
		{
			breakpoint: 769,
			settings: {
				centerMode: true,
				centerPadding: '1px',
				slidesToShow: 5,
			}
		},
		{
			breakpoint: 692,
			settings: {
				centerMode: true,
				centerPadding: '1px',
				slidesToShow: 3,
				mobileFirst: true,
			}
		},
		{
			breakpoint: 319,
			settings: {
				centerPadding: '1px',
				slidesToShow: 1,
			}
		}
	]
});


 const burger = document.querySelector('[data-burg]'),
		 menuBurger = document.querySelector('[data-menu]'),
		 burgerLink = document.querySelectorAll('.burger__link'),
		 feedbackbtn = document.querySelector('.feedback-burger'),
		 body = document.querySelector('body');
		 

		 burger.addEventListener('click', function() {
			 menuBurger.classList.toggle('hide');
			 burger.classList.toggle('active');
			 body.classList.toggle('hidden');
		 })


		 burgerLink.forEach( item => {
			item.addEventListener('click', function() {
				menuBurger.classList.toggle('hide');
				body.classList.toggle('hidden');
				burger.classList.toggle('active');
			});
		 });



const openModal = document.querySelectorAll('[data-open]'),
		 closeModal = document.querySelector('[data-close]'),
		 openBrgMod = document.querySelector('[data-brg]'),
		 modal = document.querySelector('.overlay-window');

		 openModal.forEach(item => {
		 item.addEventListener('click', function() {
		 modal.classList.toggle('hide');
		 body.classList.toggle('hidden');
		//  menuBurger.classList.remove('hide');
		//  burger.classList.remove('active');

		 });
	});

	    closeModal.addEventListener('click', function(){
			modal.classList.toggle('hide');
			body.classList.toggle('hidden');
		 });

		 openBrgMod.addEventListener('click', function () {
			menuBurger.classList.toggle('hide');
			 burger.classList.toggle('active');
			 modal.classList.toggle('hide');
		 });

const openStaff = document.getElementsByClassName('item-carousel'),
		closeStaff = document.querySelectorAll('[data-staffclose]'),
		modalStaff = document.querySelector('.overlay-slider-window'),
		director = document.getElementsByClassName('director')[0],
		setDirector = document.getElementsByClassName('setdirector'),
		subparagraph = document.getElementsByClassName('subparagraph')[0],
		setName = document.getElementsByClassName('setname'),
		textSliderModal = document.getElementsByClassName('text-slider-modal')[0],
		setContent = document.getElementsByClassName('setcontent');
		 for (let i = 0; i < openStaff.length; i++) {
			 openStaff[i].onclick = function () {
				director.innerHTML = setDirector[i].value;
				if (setDirector[i].value !='Директор') {
					director.style.color = '#12bcc1';
				} else if (setDirector[i].value === 'Зам. Директора') {
					director.style.color = '#12bcc1';
				} else {
					director.style.color = '';
				}
				subparagraph.innerHTML = setName[i].value;
				textSliderModal.innerHTML = setContent[i].value;
				modalStaff.classList.toggle('hide-staff');
				body.classList.toggle('hidden');
			 }
		 }

		closeStaff.forEach(event => {
			event.addEventListener('click', function () {
				modalStaff.classList.toggle('hide-staff');
				body.classList.toggle('hidden');
			});
		});



const openObject = document.querySelectorAll('.pictures-object-item'),
		closeObject = document.querySelectorAll('.closeobject'),
		modalObject = document.querySelector('.overlay-object-window'),
		setId = document.getElementsByClassName('setid'),
		setTitle = document.getElementsByClassName('settitle'),
		setText = document.getElementsByClassName('settext'),
		objectName = document.getElementsByClassName('object-name')[0],
		objectItemModal = document.getElementsByClassName('object-item-modal')[0];

		for(let x = 0; x < openObject.length; x++) {
			openObject[x].onclick = function () {
				objectName.innerHTML = setTitle[x].value;
				objectItemModal.innerHTML = setText[x].value;
				modalObject.classList.toggle('hide-object');
				body.classList.toggle('hidden');
			}
		}

			closeObject.forEach(ev => {
			ev.addEventListener('click', function () {
				modalObject.classList.toggle('hide-object');
				body.classList.toggle('hidden');
			});
		});
