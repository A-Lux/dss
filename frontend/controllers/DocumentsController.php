<?php
namespace frontend\controllers;

use common\models\CategoryDocuments;
use common\models\DocumentCart;
use common\models\Documents;
use common\models\SubcategoryDocuments;
use Couchbase\Document;
use Yii;
use yii\web\Controller;

/**
 * Documents controller
 */
class DocumentsController extends FrontendController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [

        ]);
    }

    public function actionView($id)
    {
        $category           = CategoryDocuments::findOne(['id' => $id]);
        $subcategory        = SubcategoryDocuments::find()->all();
        $documents          = Documents::findAll(['category_id' => $category->id]);

        return $this->render('view', [
            'category'      => $category,
            'subcategory'   => $subcategory,
            'documents'     => $documents,
        ]);
    }

    public function actionCart($id)
    {
        $category   = CategoryDocuments::findOne(['id' => $id]);
        $carts      = DocumentCart::findAll(['category_id' => $category->id]);

        return $this->render('cart', [
            'category'  => $category,
            'carts'     => $carts,
        ]);
    }

}
