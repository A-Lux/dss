<?php
namespace frontend\controllers;

use common\models\About;
use common\models\Advantages;
use common\models\CategoryDocuments;
use common\models\Gallery;
use common\models\Menu;
use common\models\Objects;
use common\models\Symbols;
use common\models\Team;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $about          = About::find()->orderBy(['id' => SORT_ASC])->one();
        $advantages     = Advantages::find()->all();
        $gallery        = Gallery::find()->all();
        $objects        = Objects::getAll();
        $team           = Team::find()->all();
        $symbols        = Symbols::find()->all();
        $menu           = Menu::getKey();
        $categoryDocuments          = CategoryDocuments::find()->all();

//        print_r("<pre>");
//        print_r($objects); die;
        return $this->render('index', [
            'about'         => $about,
            'advantages'        => $advantages,
            'gallery'           => $gallery,
            'objects'           => $objects,
            'team'              => $team,
            'symbols'           => $symbols,
            'menu'              => $menu,
            'categoryDocuments' => $categoryDocuments,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
