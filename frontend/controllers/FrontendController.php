<?php

namespace frontend\controllers;

use common\models\Banners;
use common\models\Contact;
use common\models\Language;
use common\models\Logo;
use common\models\Menu;
use common\models\Objects;
use common\models\SocialNetworks;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;

class FrontendController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function init()
    {
        $menuHeader     = Menu::getHeader();
        $menuFooter     = Menu::getFooter();
        $contacts       = Contact::getOne();
        $logoHeader     = Logo::getHeader();
        $logoFooter     = Logo::getFooter();
        $banner         = Banners::find()->orderBy(['sort' => SORT_ASC])->one();
        $socialNetworks = SocialNetworks::find()->all();
        $language       = Language::getAll();
        $object         = Objects::getBanner();


        \Yii::$app->view->params['menuHeader']      = $menuHeader;
        \Yii::$app->view->params['menuFooter']      = $menuFooter;
        \Yii::$app->view->params['contacts']        = $contacts;
        \Yii::$app->view->params['logoHeader']      = $logoHeader;
        \Yii::$app->view->params['logoFooter']      = $logoFooter;
        \Yii::$app->view->params['banner']          = $banner;
        \Yii::$app->view->params['socialNetworks']  = $socialNetworks;
        \Yii::$app->view->params['language']        = $language;
        \Yii::$app->view->params['object']          = $object;

        parent::init();

    }

    protected function setMeta($title = null, $description = null, $keywords = null)
    {
        $this->view->title      =   $title;
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
    }
}
