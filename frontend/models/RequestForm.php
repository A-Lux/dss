<?php
namespace frontend\models;

use common\models\RequestSuggestion;
use common\modules\user\models\BaseUser;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class RequestForm extends Model
{
    public $username;
    public $phone;
    public $email;
    public $message;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username'      => 'Имя',
            'phone'         => 'Телефон',
            'email'         => 'Эл. почта',
            'message'       => 'Сообщение',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'phone', 'email'], 'required'],

            [['message'], 'string'],
            [['username', 'phone'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 128],
            [['email'], 'email'],
        ];
    }

    /**
     * Create new request
     * @return bool|RequestSuggestion
     */
    public function request()
    {
        if ($this->validate()) {
            $request                    = new RequestSuggestion();
            $request->username          = $this->username;
            $request->status            = RequestSuggestion::STATUS_NOT_READ;
            $request->phone             = $this->phone;
            $request->email             = $this->email;
            $request->message           = $this->message;
            $request->save();

            return $request;
        }

        return false;
    }

}
